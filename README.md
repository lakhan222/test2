# Task Project

This Project will store Users login and logout time.
This project is developed using Python, Django and Sqlite database.

## Features

- It contains four API's. 1.Register, 2.Login, 3.Logout, 4.Adding Working Window Timestamps 5.Getting user's own login and logout data 6.Getting User data using id or username.

## Tech

This project uses multiple open source to make setup:

- [Python] - As Backend Language
- [Django] - As Backend Framework
- [Django Restframework] - For creating API's and integrating with Django
- [Sqlite Database] - As light-weight and default supported databases

## Installation

This project requires [Python3.6](https://www.python.org/downloads/release/python-360/)

Install the dependencies and devDependencies and start the server.

```sh
cd test2
pip3 install requirements.txt
python manage.py runserver
```

## Plugins

Dillinger is currently extended with the following plugins.
Instructions on how to use them in your own application are linked below.

| API | Method | Description | URL | Payload |
| ------ | ------ | ------ | ------ | ------ |
| Register | POST | For User registration | [http://127.0.0.1:8000/api/app101/register/] | {"name": "test1", "username": "test1", "password":"password123"} |
| Login |  POST | For User Login | [http://127.0.0.1:8000/api/app101/login/] | {"username": "test1", "password":"password123"} |
| Logout | GET | For User Logout(Bearer token in Authorization) | [http://127.0.0.1:8000/api/app101/logout/] |  |
| User Data | GET | Fetching current User Data(Bearer token in Authorization) | [http://127.0.0.1:8000/api/app101/auth/record/] |  |
| Working Window time set | POST | For Setting up Working Window of current date(Bearer token in Authorization) | [http://127.0.0.1:8000/api/app101/data/] | {"start_time": "2021-09-14 11:06:06", "end_time": "2021-09-14 23:06:06"} |
| Getting data of User | GET | Fetching data of login and logout of a user using id or username in query params | [http://127.0.0.1:8000/api/app101/auth/user/?id=1] |  |

Verify the deployment by navigating to your server address in
your preferred browser.

```sh
127.0.0.1:8000
```



