from django.db.models import Q
from rest_framework import permissions
from app101.models import AuthRecord


class TokenValidation(permissions.BasePermission):
    def has_permission(self, request, view):
        try:
            AuthRecord.objects.get(Q(user=request.user) & Q(logout_datetime__isnull=True) &
                                   Q(token=request.auth.decode("utf-8")))
            user_allowed = False
        except AuthRecord.DoesNotExist:
            user_allowed = True
        return user_allowed
