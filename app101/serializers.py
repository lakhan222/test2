from rest_framework import serializers

from app101.models import AuthRecord


class AccountSerializer(serializers.Serializer):
    name = serializers.CharField(required=True, min_length=6, max_length=15)
    username = serializers.CharField(required=True, min_length=6, max_length=15)
    password = serializers.CharField(required=True, min_length=5, max_length=10)
    confirm_password = serializers.CharField(write_only=True)

    def validate(self, data):
        if data['password'] != data['confirm_password']:
            raise serializers.ValidationError("passwords not match")
        return data


class AuthRecordSerializer(serializers.ModelSerializer):
    total_duration_seconds = serializers.SerializerMethodField()

    class Meta:
        model = AuthRecord
        fields = ('id', 'user', 'login_datetime', 'logout_datetime', 'created_date', "total_duration_seconds")

    def get_total_duration_seconds(self, ob):
        if ob.logout_datetime:
            return round((ob.logout_datetime - ob.login_datetime).total_seconds(), 2)
        else:
            return None


class UserAuthRecordSerializer(serializers.ModelSerializer):

    class Meta:
        model = AuthRecord
        fields = ('id', 'user', 'login_datetime', 'logout_datetime', 'created_date')
