import datetime

from django.contrib.auth import authenticate
from django.core.exceptions import ValidationError
from django.db.models import Q
from django.shortcuts import render

# Create your views here.
from django.utils.datastructures import MultiValueDictKeyError
from rest_framework import status
from rest_framework.exceptions import ParseError
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework_jwt.settings import api_settings
from rest_framework.views import APIView

from app101.models import Account, AuthRecord, WorkingWindow
from app101.permissions import TokenValidation
from app101.serializers import AccountSerializer, AuthRecordSerializer, UserAuthRecordSerializer


class CustomMessage(Exception):
    def __init__(self, message):
        self.message = message


class RegisterAPI(APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        try:
            name = request.data['name']
            username = request.data['username']
            password = request.data['password']
            if len(name) > 60:
                raise CustomMessage("Name cannot be more than 60 Characters")
            try:
                Account.objects.get(username=username)
                raise CustomMessage("username already exists")
            except Account.DoesNotExist:
                pass
            serialized = AccountSerializer(data=request.data)
            if serialized.is_valid():
                Account.objects.create_user(name=name, password=password, username=username)
            else:
                raise CustomMessage(serialized._errors)
            return Response({"data": {"is_success": True, "name": name, "message": "Account created successfully, "
                                                                                   "You can login now"}},
                            status=status.HTTP_201_CREATED)
        except CustomMessage as e:
            return Response({"data": {"is_success": False, "message": e.message}},
                            status=status.HTTP_200_OK)
        except (ParseError, ZeroDivisionError, MultiValueDictKeyError, KeyError, ValueError, ValidationError):
            return Response(status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response(
                {"status": status.HTTP_500_INTERNAL_SERVER_ERROR, "message": "fail", "raw_message": str(e)},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class LoginAPI(APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        try:
            username = request.data['username']
            password = request.data['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                user_obj = Account.objects.get(username=username)
                jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
                jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
                payload = jwt_payload_handler(user_obj)
                payload["name"] = user_obj.name
                token = jwt_encode_handler(payload)
                AuthRecord.objects.create(user=user_obj, token=token)
                return Response({"data": {"is_success": True, "message": "Login Success", "token": token}},
                                status=status.HTTP_200_OK)
            else:
                raise CustomMessage("Credentials didn't match")
        except CustomMessage as e:
            return Response({"data": {"is_success": False, "message": e.message}},
                            status=status.HTTP_200_OK)
        except (ParseError, ZeroDivisionError, MultiValueDictKeyError, KeyError, ValueError, ValidationError):
            return Response(status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response(
                {"status": status.HTTP_500_INTERNAL_SERVER_ERROR, "message": "fail", "raw_message": str(e)},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class WorkingWindowAPI(APIView):
    permission_classes = (IsAuthenticated, TokenValidation)

    def post(self, request):
        try:
            start_time = request.data['start_time']
            end_time = request.data['end_time']
            format1 = "%Y-%m-%d %H:%M:%S"
            res1 = bool(datetime.datetime.strptime(start_time, format1))
            res2 = bool(datetime.datetime.strptime(end_time, format1))
            if not res1 or not res2:
                raise CustomMessage(f"Please provide data time in format: {format1}")
            try:
                WorkingWindow.objects.get(created_date=datetime.datetime.today())
                raise CustomMessage("Working Window is already created")
            except WorkingWindow.DoesNotExist:
                pass
            WorkingWindow.objects.create(user=request.user, start_datetime=start_time, end_datetime=end_time)
            return Response({"data": {"is_success": True, "message": "Working Window Created Successfully"}},
                            status=status.HTTP_200_OK)
        except CustomMessage as e:
            return Response({"data": {"is_success": False, "message": e.message}},
                            status=status.HTTP_200_OK)
        except (ParseError, ZeroDivisionError, MultiValueDictKeyError, KeyError, ValueError, ValidationError):
            return Response(status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response(
                {"status": status.HTTP_500_INTERNAL_SERVER_ERROR, "message": "fail", "raw_message": str(e)},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class Logout(APIView):
    permission_classes = (IsAuthenticated, TokenValidation)

    def get(self, request):
        try:
            try:
                user_obj = AuthRecord.objects.get(Q(user=request.user) & Q(logout_datetime__isnull=True) &
                                                    Q(token=request.auth.decode("utf-8")))
                user_obj.logout_datetime = datetime.datetime.now()
                user_obj.save()
            except AuthRecord.DoesNotExist:
                raise CustomMessage("User is not logged in")
            return Response({"data": {"is_success": True, 'message': "Logout Successful"}},
                            status=status.HTTP_200_OK)
        except CustomMessage as e:
            return Response(status=status.HTTP_403_FORBIDDEN)
        except (ParseError, ZeroDivisionError, MultiValueDictKeyError, KeyError, ValueError, ValidationError):
            return Response(status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({"message": "fail", "error": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class AuthRecordAPI(APIView):
    permission_classes = (IsAuthenticated, )

    def get(self, request):
        try:
            try:
                ww_obj = WorkingWindow.objects.get(created_date=datetime.datetime.today())
            except WorkingWindow.DoesNotExist:
                raise CustomMessage("working window is not created")
            ww_time = (ww_obj.end_datetime - ww_obj.start_datetime).total_seconds()

            authrecord_obj = AuthRecord.objects.filter(Q(user=request.user) &
                                                          Q(created_date=datetime.datetime.today()))
            serializer_data = AuthRecordSerializer(authrecord_obj, many=True).data
            login_count = 0
            logout_count = 0
            time_remained_login = 0
            for i in authrecord_obj:
                if i.login_datetime:
                    login_count += 1
                if i.logout_datetime:
                    logout_count += 1
                if i.login_datetime and i.logout_datetime:
                    time_remained_login += round((i.logout_datetime - i.login_datetime).total_seconds(), 2)
            if time_remained_login >= ww_time:
                attendance = "Present"
            else:
                attendance = "Absent"
            return Response({"data": {"is_success": True, 'data': serializer_data, "login_count": login_count,
                                      "logout_count": logout_count, "attendance": attendance}},
                            status=status.HTTP_200_OK)
        except CustomMessage as e:
            return Response({"data": {"is_success": False, "message": e.message}},
                            status=status.HTTP_200_OK)
        except (ParseError, ZeroDivisionError, MultiValueDictKeyError, KeyError, ValueError, ValidationError):
            return Response(status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({"message": "fail", "error": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class UserAuthRecordAPI(APIView):
    permission_classes = (AllowAny, )

    def get(self, request):
        try:
            user_id = request.query_params.get("id")
            user_name = request.query_params.get("user_name")
            if not user_id and not user_name:
                raise CustomMessage("provide id or username")
            if user_id:
                try:
                    user_obj = Account.objects.get(id=user_id)
                except Account.DoesNotExist:
                    raise CustomMessage("id is invalid")
            else:
                try:
                    user_obj = Account.objects.get(username=user_name)
                except Account.DoesNotExist:
                    raise CustomMessage("username is invalid")

            authrecord_obj = AuthRecord.objects.filter(user=user_obj)
            serializer_data = UserAuthRecordSerializer(authrecord_obj, many=True).data
            login_count = 0
            logout_count = 0
            for i in authrecord_obj:
                if i.login_datetime:
                    login_count += 1
                if i.logout_datetime:
                    logout_count += 1

            return Response({"data": {"is_success": True, 'data': serializer_data, "login_count": login_count,
                                      "logout_count": logout_count}},
                            status=status.HTTP_200_OK)
        except CustomMessage as e:
            return Response({"data": {"is_success": False, "message": e.message}},
                            status=status.HTTP_200_OK)
        except (ParseError, ZeroDivisionError, MultiValueDictKeyError, KeyError, ValueError, ValidationError):
            return Response(status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({"message": "fail", "error": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
