from django.urls import path

from app101 import views

urlpatterns = [
    path('register/', views.RegisterAPI.as_view()),
    path('login/', views.LoginAPI.as_view()),
    path('logout/', views.Logout.as_view()),
    path('wwindow/', views.WorkingWindowAPI.as_view()),
    path('auth/record/', views.AuthRecordAPI.as_view()),
    path('auth/user/', views.UserAuthRecordAPI.as_view()),
]

