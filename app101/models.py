import datetime
import jwt
from django.contrib.auth.base_user import BaseUserManager, AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.db import models


class UserManager(BaseUserManager):
    def create_user(self, name, password, username):
        user = self.model(name=name, username=username)
        user.set_password(password)
        user.is_active = True
        user.save()
        return user

    def create_superuser(self, password, name, username):
        user = self.model(name=name, username=username)
        user.set_password(password)
        user.is_superuser = True
        user.is_staff = True
        user.is_active = True
        user.save()
        return user


class Account(AbstractBaseUser, PermissionsMixin):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=60)
    is_active = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    signup_date = models.DateField(auto_now_add=True)
    username = models.CharField(max_length=100, unique=True)

    USERNAME_FIELD = 'username'
    objects = UserManager()

    def __str__(self):
        return "{}".format(self.id)

    def __id__(self):
        return self.id

    class Meta:
        db_table = 'account'
        managed = True


class WorkingWindow(models.Model):
    user = models.ForeignKey(Account, on_delete=models.DO_NOTHING)
    start_datetime = models.DateTimeField(null=True, blank=True)
    end_datetime = models.DateTimeField(null=True, blank=True)
    created_date = models.DateField(auto_now_add=True)

    class Meta:
        db_table = 'working_window'
        managed = True


class AuthRecord(models.Model):
    user = models.ForeignKey(Account, on_delete=models.DO_NOTHING)
    login_datetime = models.DateTimeField(auto_now_add=True)
    logout_datetime = models.DateTimeField(null=True, blank=True)
    token = models.TextField(null=False, blank=False)
    created_date = models.DateField(auto_now_add=True)

    class Meta:
        db_table = 'auth_record'
        managed = True
